# Search Protocol - Group 64

Search string

~~~~~ {.searchstring }
(((security risk) AND (internet connected devices) OR (threat to people) AND (public environment)))
~~~~~

Number of papers:208 

Inclusion criteria:

1.papers after 2005 to till date
2.journal and conference papers of threat analysis
3.papers related to IOT
4.papers with security risk of public environment
5.evidance based research papers

Exclusion criteria:

1.papers which is not about security and public security
2.papers involes other methodology

